# hs-lintimespace

Linear time and space kernelization for d-Hitting Set(k) problem.
The python package `hs-lintimespace` consists of some reducers
for the problem and datasets.
The initial puprpose of the library is to conduct experiments
on data reduction effectiveness.

Van Bevern kernelization, Fafianie and Kratsch kernelization,
and also Weihe reducer are implemented in C++ and used in python library
via a Cython wrapping.
The C++ implementations can be found in `python-package/source`.

This is a supplementary package for an algorithmic research project.
For algorithm description and proved efficiency estimates see
[the article](https://doi.org/10.1016/j.ipl.2020.105998)
or the developer's Bachelor thesis `ba-thesis.pdf`.

# Installation

The library is intended to run on MacOS or Linux.
Prerequisities:

- `Python` (version 3.7 or higher)
- `pip` (comes with Python)
- `boost` C++ library

Install the package by running:

`pip install python-package`

# Example

In order to see some examples of usage,
install Jupyter Notebook with:

`pip install jupyter`

Then from `example` directory run:

`jupyter-notebook example.ipynb`

# Acknowledgments

This tool was created within project 18-501-12031,
"Trade-offs in parameterized data reduction"
of the [Russian Foundation for Basic Research](https://www.rfbr.ru/rffi/ru/).
