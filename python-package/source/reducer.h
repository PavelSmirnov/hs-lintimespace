#pragma once
#include "timer.h"
#include <fstream>
#include <sstream>
#include <string>
#include <vector>


namespace hittingset {
    using ulong = unsigned long long;
    using uint = unsigned int;
    using Vertex = uint;
    using Edge = std::vector<Vertex>;
    using Hypergraph = std::vector<Edge>;

    struct ReductionResult {
        Hypergraph instance;
        double time_in_sec;
        std::string log;
    };

    class MaxHSParameterizedReducer {
    public:
        ReductionResult Reduce(const Hypergraph& instance,
                               const uint max_hitting_set,
                               const uint max_all_subsets_edge) const {
            ReductionResult result;
            std::stringstream logger;
            Timer timer;
            Reduce(instance, max_hitting_set, max_all_subsets_edge, &result.instance, &logger);
            result.time_in_sec = timer.GetTime();
            result.log = logger.str();
            return result;
        }

        virtual ~MaxHSParameterizedReducer() {}
    protected:
        virtual void Reduce(const Hypergraph& instance,
                            const uint max_hitting_set,
                            const uint max_all_subsets_edge,
                            Hypergraph* result,
                            std::ostream* logger) const = 0;
    };
} // namespace hittingset
