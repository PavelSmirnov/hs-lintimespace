#pragma once
#include "reducer.h"
#include "timer.h"
#include <sstream>
#include <vector>


namespace hittingset {
    class WeiheReducer {
    public:
        ReductionResult Reduce(const Hypergraph& instance) const {
            ReductionResult result;
            std::stringstream logger;
            Timer timer;
            Reduce(instance, &result.instance, &logger);
            result.time_in_sec = timer.GetTime();
            result.log = logger.str();
            return result;
        }

        void Reduce(const Hypergraph& instance, Hypergraph* result, std::ostream* logger) const;
    };
} // namespace hittingset
