#pragma once
#include "helpers.h"
#include "reducer.h"
#include <map>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace hittingset {
    struct CoreMarks {
        std::unordered_set<Vertex> petal_vertices;
        uint num_petals = 0;
    };

    template <class EdgeIndexedMap>
    class VanBevernSimpleReducer : public MaxHSParameterizedReducer {
    public:
        using MaxHSParameterizedReducer::Reduce;
    protected:
        void Reduce(const Hypergraph& instance,
                    const uint max_hitting_set,
                    const uint max_all_subsets_edge,
                    Hypergraph* kernel,
                    std::ostream* logger) const override {
            assert(kernel);
            assert(logger);
            assert(max_all_subsets_edge < 33);

            *logger << "number of edges = " << instance.size() << std::endl;

            auto sorted_instance = SortEdgesVertices(instance);
            sorted_instance = SortEdgesBySize(sorted_instance);

            uint max_edge_size;
            uint max_vertex;
            CalcMaxEdgeSizeAndMaxVertex(sorted_instance, &max_edge_size, &max_vertex);

            EdgeIndexedMap core_marks;
            for (const auto& edge : sorted_instance) {
                const std::vector<Edge> subsets
                    = GetSubsets(edge, instance, max_all_subsets_edge, max_vertex);

                bool take = true;
                for (const auto& subset : subsets) {
                    if (core_marks[subset].num_petals > max_hitting_set) {
                        take = false;
                        break;
                    }
                }
                if (!take) {
                    continue;
                }

                kernel->push_back(edge);

                for (const auto& subset : subsets) {
                    auto& marks = core_marks[subset];

                    if (edge.size() == subset.size()) {
                      marks.num_petals = max_hitting_set + 1; // to remove all supersets
                      continue;
                    }

                    std::vector<Vertex> petal;
                    std::set_difference(edge.begin(),
                                        edge.end(),
                                        subset.begin(),
                                        subset.end(),
                                        std::back_inserter(petal));

                    bool add_petal = true;
                    for (const Vertex vertex : petal) {
                        if (marks.petal_vertices.find(vertex) != marks.petal_vertices.end()) {
                            add_petal = false;
                            break;
                        }
                    }

                    if (!add_petal) {
                        continue;
                    }

                    ++marks.num_petals;
                    for (const Vertex vertex : petal) {
                        marks.petal_vertices.insert(vertex);
                    }
                }
            }
            *logger << "kernel size = " << kernel->size() << std::endl;
        }
    };

    using VanBevernHashingReducer =
        VanBevernSimpleReducer<std::unordered_map<Edge, CoreMarks, EdgeHasher>>;

    using VanBevernMappingReducer =
        VanBevernSimpleReducer<std::map<Edge, CoreMarks, EdgeComparator>>;

    using VanBevernWithTrieReducer = VanBevernSimpleReducer<EdgeTrie<CoreMarks>>;

    using VanBevernWithMapTrieReducer =
        VanBevernSimpleReducer<EdgeTrie<CoreMarks, std::map<Vertex, ulong>>>;
} // namespace hittingset
