#include "fafianie_kratsch.h"
#include "helpers.h"
#include "reducer.h"
#include <algorithm>
#include <cassert>
#include <iterator>

namespace hittingset {
    namespace {
        struct SubsetQuery {
            Edge subset;
            uint edge_id;
        };

        void GetAllSubsetsAsQueries(const Edge& edge,
                                    const uint edge_id,
                                    std::vector<SubsetQuery>* queries) {
            assert(queries);

            const uint size = edge.size();
            for (Mask mask = 0; mask <= GetFullMask(size); ++mask) {
                auto& query = queries->emplace_back(SubsetQuery{{}, edge_id});
                for (uint pos = 0; pos < size; ++pos) {
                    if (HasBit(mask, pos)) {
                        query.subset.push_back(edge[pos]);
                    }
                }
            }
        }

        void GetIntersectionsAsQueries(const Hypergraph& instance,
                                       const uint max_vertex,
                                       const uint edge_id,
                                       std::vector<SubsetQuery>* queries,
                                       std::ostream* logger) {
            assert(queries);

            std::vector<std::vector<uint>> subsets;
            subsets.reserve(instance.size());
            const auto& edge = instance[edge_id];
            for (const auto& other : instance) {
                auto& subset = subsets.emplace_back();
                std::set_intersection(edge.begin(),
                                      edge.end(),
                                      other.begin(),
                                      other.end(),
                                      std::back_inserter(subset));
            }
            for (const uint vertex : edge) {
                subsets.push_back({vertex});
            }

            auto vertex_accessor = [&](const uint id, const uint pos) -> uint {
                auto& subset = subsets[id];
                return subset.size() > pos ? subset[pos] : max_vertex + 1;
            };
            const auto permutation = SortRadix(subsets.size(), edge.size(), max_vertex + 1, vertex_accessor);

            for (uint subset_id = 0; subset_id < subsets.size(); ++subset_id) {
                const auto& subset = subsets[permutation[subset_id]];
                if (subset_id == 0 || subset != subsets[permutation[subset_id - 1]]) {
                    queries->emplace_back(SubsetQuery{subset, edge_id});
                }
            }
        }

        std::vector<SubsetQuery> GetSubsetQueries(const Hypergraph& instance,
                                                  const uint max_vertex,
                                                  const uint max_all_subsets_edge,
                                                  std::ostream* logger) {
            (*logger) << "counting queries with max all subsets edge = " << max_all_subsets_edge << std::endl;
            std::vector<SubsetQuery> queries;
            uint big_edges = 0;
            for (uint edge_id = 0; edge_id < instance.size(); ++edge_id) {
                const auto& edge = instance[edge_id];
                if (edge.size() <= max_all_subsets_edge) {
                    GetAllSubsetsAsQueries(edge, edge_id, &queries);
                } else {
                    GetIntersectionsAsQueries(instance, max_vertex, edge_id, &queries, logger);
                    ++big_edges;
                }
            }
            (*logger) << "number of big edges = " << big_edges << std::endl;
            return queries;
        }

        void OptimizeSubsetQueries(const std::vector<SubsetQuery>& queries,
                                   const uint num_edges,
                                   const uint max_edge_size,
                                   const uint max_vertex,
                                   std::vector<std::vector<uint>>* subset_ids_by_edge,
                                   std::vector<uint>* subset_sizes,
                                   std::ostream* logger) {
            assert(subset_ids_by_edge);
            assert(subset_sizes);

            // TODO: custom sort for large queries

            auto vertex_accessor = [&](const uint id, const uint pos) -> uint {
                auto& subset = queries[id].subset;
                return subset.size() > pos ? subset[pos] : max_vertex + 1;
            };
            auto permutation = SortRadix(queries.size(), max_edge_size, max_vertex + 1, vertex_accessor);

            uint subsets_num = 0;
            subset_ids_by_edge->resize(num_edges);
            for (uint pos = 0; pos < permutation.size(); ++pos) {
                auto& query = queries[permutation[pos]];
                if (pos == 0 || queries[permutation[pos - 1]].subset != query.subset) {
                    ++subsets_num;
                    subset_sizes->push_back(query.subset.size());
                }
                (*subset_ids_by_edge)[query.edge_id].push_back(subsets_num - 1);
            }
        }

        void ApplySubsetQueries(const Hypergraph& instance,
                                const uint max_hitting_set,
                                const uint max_edge_size,
                                const std::vector<std::vector<uint>>& subset_ids_by_edge,
                                const std::vector<uint>& subset_sizes,
                                Hypergraph* kernel_ptr,
                                std::ostream* logger_ptr) {
            assert(kernel_ptr);
            assert(logger_ptr);

            auto& logger = *logger_ptr;

            const auto superset_bounds = CalcFKSupersetBounds(max_hitting_set,
                                                              max_edge_size,
                                                              instance.size());
            logger << "kernel upper bound = " << superset_bounds[0] << std::endl;

            std::vector<uint> supersets(subset_sizes.size(), 0);
            for (uint edge_id = 0; edge_id < instance.size(); ++edge_id) {
                bool take = true;
                for (const auto subset_id : subset_ids_by_edge[edge_id]) {
                    if (supersets[subset_id] >= superset_bounds[subset_sizes[subset_id]]) {
                        take = false;
                        break;
                    }
                }
                if (!take) {
                    continue;
                }

                kernel_ptr->emplace_back(instance[edge_id]);
                for (const auto subset_id : subset_ids_by_edge[edge_id]) {
                    if (subset_sizes[subset_id] == instance[edge_id].size()) { // to remove superset edges
                        supersets[subset_id] = superset_bounds[supersets[subset_id]];
                        continue;
                    }
                    ++supersets[subset_id];
                }
            }
        }
    } // namespace anonymous

    void FafianieKratschReducer::Reduce(const Hypergraph& instance,
                                        const uint max_hitting_set,
                                        const uint max_all_subsets_edge,
                                        Hypergraph* kernel_ptr,
                                        std::ostream* logger_ptr) const {
        assert(kernel_ptr);
        assert(logger_ptr);
        assert(max_all_subsets_edge < 33);

        auto& logger = *logger_ptr;

        logger << "number of edges = " << instance.size() << std::endl;
        uint max_edge_size = 0;
        uint max_vertex = 0;
        for (const auto& edge : instance) {
            max_edge_size = std::max<uint>(max_edge_size, edge.size());
            for (auto vertex : edge) {
                max_vertex = std::max<uint>(max_vertex, vertex);
            }
        }
        logger << "max edge size = " << max_edge_size << std::endl;

        auto sorted_instance = SortEdgesVertices(instance, max_vertex);
        sorted_instance = SortEdgesBySize(sorted_instance);
        logger << "sorted edges" << std::endl;
        auto queries = GetSubsetQueries(sorted_instance, max_vertex, max_all_subsets_edge, logger_ptr);
        logger << "number of queries = " << queries.size() << std::endl;

        std::vector<std::vector<uint>> subset_ids_by_edge;
        std::vector<uint> subset_sizes;
        OptimizeSubsetQueries(queries,
                              sorted_instance.size(),
                              max_edge_size,
                              max_vertex,
                              &subset_ids_by_edge,
                              &subset_sizes,
                              logger_ptr);
        logger << "number of different subsets = " << subset_sizes.size() << std::endl;

        ApplySubsetQueries(sorted_instance,
                           max_hitting_set,
                           max_edge_size,
                           subset_ids_by_edge,
                           subset_sizes,
                           kernel_ptr,
                           logger_ptr);
        logger << "kernel size = " << kernel_ptr->size() << std::endl;
    }
} // namespace hittingset
