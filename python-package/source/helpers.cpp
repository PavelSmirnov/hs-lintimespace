#include "helpers.h"

namespace hittingset {
    Hypergraph SortEdgesVertices(const Hypergraph& instance, const Vertex max_vertex) {
        std::vector<std::vector<uint>> edge_ids_by_vertex(max_vertex + 1);
        for (uint edge_id = 0; edge_id < instance.size(); ++edge_id) {
            for (const auto vertex : instance[edge_id]) {
                edge_ids_by_vertex[vertex].push_back(edge_id);
            }
        }

        Hypergraph sorted(instance.size());
        for (Vertex vertex = 0; vertex <= max_vertex; ++vertex) {
            for (const auto edge_id : edge_ids_by_vertex[vertex]) {
                sorted[edge_id].push_back(vertex);
            }
        }

        return sorted;
    }

    Hypergraph SortEdgesVertices(const Hypergraph& instance) {
        Vertex max_vertex = 0;
        for (const auto& edge : instance) {
            for (const auto vertex : edge) {
                max_vertex = std::max(max_vertex, vertex);
            }
        }
        return SortEdgesVertices(instance, max_vertex);
    }

    Hypergraph SortEdgesBySize(const Hypergraph& instance) {
        std::vector<std::vector<int>> edge_ids_by_size;
        for (uint edge_id = 0; edge_id < instance.size(); ++edge_id) {
            const uint size = instance[edge_id].size();
            if (size >= edge_ids_by_size.size()) {
                edge_ids_by_size.resize(size + 1);
            }
            edge_ids_by_size[size].push_back(edge_id);
        }

        Hypergraph sorted;
        for (const auto& edge_ids : edge_ids_by_size) {
            for (const uint edge_id : edge_ids) {
                sorted.push_back(instance[edge_id]);
            }
        }

        return sorted;
    }

    std::vector<ulong> CalcFKSupersetBounds(const uint max_hitting_set,
                                            const uint max_edge_size,
                                            const uint num_edges) {
        std::vector<ulong> superset_bounds(max_edge_size + 1);
        const ulong base = max_hitting_set + 1;
        superset_bounds[0] = 1;
        for (uint exponent = 1; exponent <= max_edge_size; ++exponent) {
            superset_bounds[exponent] = std::min<ulong>(num_edges,
                                                        base * superset_bounds[exponent - 1]);
        }
        std::reverse(superset_bounds.begin(), superset_bounds.end());
        return superset_bounds;
    }

    std::vector<Edge> GetAllSubsets(const Edge& edge) {
        std::vector<Edge> subsets;
        const uint size = edge.size();
        for (Mask mask = 0; mask <= GetFullMask(size); ++mask) {
            auto& subset = subsets.emplace_back();
            for (uint pos = 0; pos < size; ++pos) {
                if (HasBit(mask, pos)) {
                    subset.push_back(edge[pos]);
                }
            }
        }
        return subsets;
    }

    std::vector<Edge> GetIntersections(const Hypergraph& instance,
                                       const uint max_vertex,
                                       const Edge& edge) {
        std::vector<Edge> subsets;
        subsets.reserve(instance.size());
        for (const auto& other : instance) {
            auto& subset = subsets.emplace_back();
            std::set_intersection(edge.begin(),
                                  edge.end(),
                                  other.begin(),
                                  other.end(),
                                  std::back_inserter(subset));
        }
        for (const uint vertex : edge) {
            subsets.push_back({vertex});
        }

        auto vertex_accessor = [&](const uint id, const uint pos) -> uint {
            auto& subset = subsets[id];
            return subset.size() > pos ? subset[pos] : max_vertex + 1;
        };
        const auto permutation = SortRadix(subsets.size(), edge.size(), max_vertex + 1, vertex_accessor);

        std::vector<Edge> result;
        for (uint subset_id = 0; subset_id < subsets.size(); ++subset_id) {
            const auto& subset = subsets[permutation[subset_id]];
            if (subset_id == 0 || subset != subsets[permutation[subset_id - 1]]) {
                result.emplace_back(subset);
            }
        }
        return result;
    }

    std::vector<Edge> GetSubsets(const Edge& edge,
                                 const Hypergraph& instance,
                                 const uint max_all_subsets_edge,
                                 const Vertex max_vertex) {
        if (edge.size() > max_all_subsets_edge) {
            return GetIntersections(instance, max_vertex, edge);
        }
        return GetAllSubsets(edge);
    }

    void CalcMaxEdgeSizeAndMaxVertex(const Hypergraph& instance,
                                     uint* max_edge_size,
                                     uint* max_vertex) {
        *max_edge_size = 0;
        *max_vertex = 0;
        for (const auto& edge : instance) {
            *max_edge_size = std::max<uint>(*max_edge_size, edge.size());
            for (auto vertex : edge) {
                *max_vertex = std::max<uint>(*max_vertex, vertex);
            }
        }
    }
} // namespace hittingset
