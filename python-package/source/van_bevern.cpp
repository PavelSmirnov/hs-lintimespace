#include "helpers.h"
#include "reducer.h"
#include "van_bevern.h"
#include <algorithm>
#include <cassert>
#include <climits>
#include <iterator>
#include <numeric>

using std::max;
using std::vector;
using std::string;

namespace hittingset {
    namespace {
        struct CoreQuery {
            Edge core;
            vector<Vertex> vertices;
            uint edge_id;
        };

        struct UsedQuery {
            uint core_id;
            Vertex vertex;
            uint edge_id;
        };

        void GetAllSubsetsAsQueries(const Edge& edge,
                                    const uint edge_id,
                                    const uint max_vertex,
                                    std::vector<CoreQuery>* subset_queries) {
            assert(subset_queries);

            const uint size = edge.size();
            const auto full_mask = GetFullMask(size);
            for (Mask mask = 0; mask <= full_mask; ++mask) {
                std::vector<uint> core = {};
                std::vector<uint> outer = {};
                for (uint pos = 0; pos < size; ++pos) {
                    if (HasBit(mask, pos)) {
                        core.push_back(edge[pos]);
                    } else {
                        outer.push_back(edge[pos]);
                    }
                }
                if (outer.empty()) {
                    outer = {max_vertex + 1};
                }
                subset_queries->emplace_back(CoreQuery{core, outer, edge_id});
            }
        }

        void GetIntersectionsAsQueries(const Hypergraph& instance,
                                       const uint max_vertex,
                                       const uint edge_id,
                                       std::vector<CoreQuery>* subset_queries,
                                       std::ostream* logger) {
            assert(subset_queries);

            const auto& edge = instance[edge_id];
            std::vector<std::vector<uint>> subsets = {{}};
            subsets.reserve(instance.size() + edge.size() + 1);
            for (const auto& other : instance) {
                auto& subset = subsets.emplace_back();
                std::set_intersection(edge.begin(),
                                      edge.end(),
                                      other.begin(),
                                      other.end(),
                                      std::back_inserter(subset));
            }
            for (const uint vertex : edge) {
                subsets.push_back({vertex});
            }

            auto vertex_accessor = [&](const uint id, const uint pos) -> uint {
                auto& subset = subsets[id];
                return subset.size() > pos ? subset[pos] : max_vertex + 1;
            };
            const auto permutation = SortRadix(subsets.size(), edge.size(), max_vertex + 1, vertex_accessor);

            for (uint subset_id = 0; subset_id < subsets.size(); ++subset_id) {
                const auto& core = subsets[permutation[subset_id]];
                if (subset_id != 0 && core == subsets[permutation[subset_id - 1]]) {
                    continue;
                }

                std::vector<uint> outer;
                std::set_difference(edge.begin(), edge.end(), core.begin(), core.end(), std::back_inserter(outer));
                if (outer.empty()) {
                    outer = {max_vertex + 1};
                }
                subset_queries->emplace_back(CoreQuery{core, outer, edge_id});
            }
        }

        void GetSubsetQueries(const Hypergraph& instance,
                              const uint max_vertex,
                              const uint max_all_subsets_edge,
                              std::vector<CoreQuery>* subset_queries,
                              std::ostream* logger) {
            assert(subset_queries);

            uint big_edges = 0;
            for (uint edge_id = 0; edge_id < instance.size(); ++edge_id) {
                const auto& edge = instance[edge_id];
                if (edge.size() <= max_all_subsets_edge) {
                    GetAllSubsetsAsQueries(edge, edge_id, max_vertex, subset_queries);
                } else {
                    GetIntersectionsAsQueries(instance, max_vertex, edge_id, subset_queries, logger);
                    ++big_edges;
                }
            }
            (*logger) << "number of big edges = " << big_edges << std::endl;
        }

        void ConvertSubsetQueriesToUsedQueries(const Hypergraph& instance,
                                               const std::vector<CoreQuery>& subset_queries,
                                               const uint max_edge_size,
                                               const uint max_vertex,
                                               uint* num_cores,
                                               std::vector<uint>* max_core_ids_by_edge,
                                               std::vector<UsedQuery>* used_queries) {
            assert(used_queries);
            assert(max_core_ids_by_edge);
            assert(num_cores);

            auto vertex_accessor = [&](const uint id, const uint pos) -> uint {
                auto& core = subset_queries[id].core;
                return core.size() > pos ? core[pos] : max_vertex + 1;
            };
            auto permutation = SortRadix(subset_queries.size(),
                                         max_edge_size,
                                         max_vertex + 1,
                                         vertex_accessor);

            *num_cores = 0;
            max_core_ids_by_edge->resize(instance.size());
            for (uint pos = 0; pos < permutation.size(); ++pos) {
                const auto& query = subset_queries[permutation[pos]];
                if (pos == 0 || subset_queries[permutation[pos - 1]].core != query.core) {
                    ++(*num_cores);
                }

                const uint core_id = (*num_cores) - 1;
                for (const Vertex vertex : query.vertices) {
                    (*used_queries).push_back(UsedQuery{core_id, vertex, query.edge_id});
                }

                const uint edge_id = query.edge_id;
                if (instance[edge_id].size() == query.core.size()) {
                    (*max_core_ids_by_edge)[edge_id] = core_id;
                }
            }
        }

        void OptimizeUsedQueries(const Hypergraph& instance,
                                 const std::vector<UsedQuery>& used_queries,
                                 const uint max_core_size,
                                 const uint max_vertex,
                                 const uint precalced_num_cores,
                                 std::vector<std::vector<uint>>* core_ids_by_edge,
                                 std::vector<std::vector<std::vector<uint>>>* used_ids_by_core_by_edge,
                                 uint* num_used,
                                 std::ostream* logger) {
            assert(core_ids_by_edge);
            assert(used_ids_by_core_by_edge);
            assert(num_used);
            assert(logger);

            auto sort_by_vertex = SortRadix(used_queries.size(),
                                            /*element_size*/1,
                                            /*max_value*/max_vertex + 1,
                                            [&](const uint id, const uint/*pos==0*/) {
                return used_queries[id].vertex;
            });

            auto sort_by_core = SortRadix(used_queries.size(),
                                          /*element_size*/1,
                                          /*max_value*/precalced_num_cores - 1,
                                          [&](const uint id, const uint/*pos==0*/) {
                return used_queries[sort_by_vertex[id]].core_id;
            });

            std::vector<uint> permutation(used_queries.size());
            for (uint index = 0; index < used_queries.size(); ++index) {
              permutation[index] = sort_by_vertex[sort_by_core[index]];
            }

            uint num_cores = 0;
            (*num_used) = 0;
            const auto num_edges = instance.size();
            core_ids_by_edge->resize(num_edges);
            used_ids_by_core_by_edge->resize(num_edges);
            for (uint pos = 0; pos < permutation.size(); ++pos) {
                auto& query = used_queries[permutation[pos]];

                if (pos == 0) {
                    num_cores = 1;
                    (*num_used) = 1;
                } else {
                    auto& prev_query = used_queries[permutation[pos - 1]];
                    if (query.core_id != prev_query.core_id) {
                        ++num_cores;
                        ++(*num_used);
                    } else if (query.vertex != prev_query.vertex) {
                        ++(*num_used);
                    }
                }

                const uint edge_id = query.edge_id;
                auto& core_ids = (*core_ids_by_edge)[edge_id];
                auto& used_ids = (*used_ids_by_core_by_edge)[edge_id];
                if (core_ids.empty() || core_ids.back() != num_cores - 1) {
                    core_ids.push_back(num_cores - 1);
                    used_ids.emplace_back();
                }
                if (query.vertex != max_vertex + 1) {
                    used_ids.back().push_back((*num_used) - 1);
                }
            }
        }

        void ApplySubsetQueries(const Hypergraph& instance,
                                const uint max_hitting_set,
                                const uint num_cores,
                                const uint num_used,
                                const std::vector<uint>& max_core_ids_by_edge,
                                const std::vector<std::vector<uint>>& core_ids_by_edge,
                                const std::vector<std::vector<std::vector<uint>>>& used_ids_by_core_by_edge,
                                Hypergraph* kernel,
                                std::ostream* logger) {
            assert(kernel);
            assert(logger);

            std::vector<uint> petals(num_cores, 0);
            std::vector<bool> used(num_used, false);
            for (uint edge_id = 0; edge_id < instance.size(); ++edge_id) {
                bool take = true;
                const auto& core_ids = core_ids_by_edge[edge_id];
                for (const auto core_id : core_ids) {
                    if (petals[core_id] > max_hitting_set) {
                        take = false;
                        break;
                    }
                }
                if (!take) {
                    continue;
                }

                kernel->push_back(instance[edge_id]);
                const auto& used_ids_by_core = used_ids_by_core_by_edge[edge_id];
                for (uint core_pos = 0; core_pos < core_ids.size(); ++core_pos) {
                    const uint core_id = core_ids[core_pos];
                    const auto& used_ids = used_ids_by_core[core_pos];

                    bool add_petal = true;
                    for (const auto used_id : used_ids) {
                        if (used[used_id]) {
                            add_petal = false;
                            break;
                        }
                    }
                    if (!add_petal) {
                        continue;
                    }

                    ++petals[core_id];
                    for (const auto used_id : used_ids) {
                        used[used_id] = true;
                    }
                }

                petals[max_core_ids_by_edge[edge_id]] = max_hitting_set + 1; // to remove superset edges
            }
        }
    } // namespace anonymous

    void VanBevernReducer::Reduce(const Hypergraph& instance,
                                  const uint max_hitting_set,
                                  const uint max_all_subsets_edge,
                                  Hypergraph* kernel,
                                  std::ostream* logger) const {
        assert(kernel);
        assert(logger);
        assert(max_all_subsets_edge < 33);

        (*logger) << "number of edges = " << instance.size() << std::endl;
        uint max_edge_size = 0;
        uint max_vertex = 0;
        for (const auto& edge : instance) {
            max_edge_size = std::max<uint>(max_edge_size, edge.size());
            for (auto vertex : edge) {
                max_vertex = std::max<uint>(max_vertex, vertex);
            }
        }
        (*logger) << "max edge size = " << max_edge_size << std::endl;

        auto sorted_instance = SortEdgesVertices(instance, max_vertex);
        sorted_instance = SortEdgesBySize(sorted_instance);
        std::vector<CoreQuery> subset_queries;
        GetSubsetQueries(sorted_instance,
                         max_vertex,
                         max_all_subsets_edge,
                         &subset_queries,
                         logger);
        (*logger) << "number of subset queries = " << subset_queries.size() << std::endl;

        std::vector<UsedQuery> used_queries;
        std::vector<uint> max_core_ids_by_edge;
        uint num_cores;
        ConvertSubsetQueriesToUsedQueries(sorted_instance,
                                          subset_queries,
                                          max_edge_size,
                                          max_vertex,
                                          &num_cores,
                                          &max_core_ids_by_edge,
                                          &used_queries);
        (*logger) << "number of used queries = " << used_queries.size() << std::endl;


        std::vector<std::vector<uint>> core_ids_by_edge;
        std::vector<std::vector<std::vector<uint>>> used_ids_by_core_by_edge;
        uint num_used;
        OptimizeUsedQueries(sorted_instance,
                            used_queries,
                            max_edge_size,
                            max_vertex,
                            num_cores,
                            &core_ids_by_edge,
                            &used_ids_by_core_by_edge,
                            &num_used,
                            logger);
        (*logger) << "number of unique used queries = " << num_used << std::endl;

        ApplySubsetQueries(sorted_instance,
                           max_hitting_set,
                           num_cores,
                           num_used,
                           max_core_ids_by_edge,
                           core_ids_by_edge,
                           used_ids_by_core_by_edge,
                           kernel,
                           logger);
        (*logger) << "kernel size = " << kernel->size() << std::endl;
    }
} // namespace hittingset
