#pragma once
#include <ctime>

namespace hittingset {
    class Timer {
    public:
        Timer() {
            _zero_time = clock();
        }

        double GetTime() const {
            return double(clock() - _zero_time) / CLOCKS_PER_SEC;
        }
    private:
        clock_t _zero_time;
    };
}
