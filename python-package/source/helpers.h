#pragma once
#include "reducer.h"
#include <algorithm>
#include <numeric>
#include <unordered_map>
#include <vector>

#include <boost/container_hash/hash.hpp>

namespace hittingset {
    struct EdgeHasher {
        size_t operator()(const Edge& edge) const {
            return boost::hash_value(edge);
        }
    };

    struct EdgeComparator {
        bool operator()(const Edge& lhs, const Edge& rhs) const {
            if (lhs.size() < rhs.size()) {
              return true;
            }
            if (lhs.size() > rhs.size()) {
              return false;
            }
            for (uint pos = 0; pos < lhs.size(); ++pos) {
              if (lhs[pos] < rhs[pos]) {
                return true;
              }
              if (lhs[pos] > rhs[pos]) {
                return false;
              }
            }
            return false;
        }
    };

    template <class ValueAccessor>
    std::vector<uint> SortRadix(const uint num_elements,
                                const uint element_size,
                                const uint max_value,
                                const ValueAccessor& value_accessor) {
        std::vector<uint> permutation(num_elements);
        std::iota(permutation.begin(), permutation.end(), 0);

        for (uint phase = 0; phase < element_size; ++phase) {
            const uint pos = element_size - 1 - phase;
            std::vector<std::vector<uint>> buckets(max_value + 1);
            for (const uint element : permutation) {
                buckets[value_accessor(element, pos)].push_back(element);
            }

            permutation.clear();
            for (const auto& bucket : buckets) {
                for (const uint element : bucket) {
                    permutation.push_back(element);
                }
            }
        }

        return permutation;
    }

    using Mask = uint;

    inline Mask GetFullMask(const uint size) {
        return (static_cast<Mask>(1) << size) - 1;
    }

    inline bool HasBit(const Mask mask, const uint pos) {
        return mask & static_cast<Mask>(1) << pos;
    }

    Hypergraph SortEdgesVertices(const Hypergraph& instance, const Vertex max_vertex);

    Hypergraph SortEdgesVertices(const Hypergraph& instance);

    Hypergraph SortEdgesBySize(const Hypergraph& instance);

    std::vector<ulong> CalcFKSupersetBounds(const uint max_hitting_set,
                                            const uint max_edge_size,
                                            const uint num_edges);

    std::vector<Edge> GetAllSubsets(const Edge& edge);

    std::vector<Edge> GetIntersections(const Hypergraph& instance,
                                       const uint max_vertex,
                                       const Edge& edge);

    std::vector<Edge> GetSubsets(const Edge& edge,
                                 const Hypergraph& instance,
                                 const uint max_all_subsets_edge,
                                 const Vertex max_vertex);

    void CalcMaxEdgeSizeAndMaxVertex(const Hypergraph& instance,
                                     uint* max_edge_size,
                                     uint* max_vertex);

    template <class Data, class TransitionsMap = std::unordered_map<Vertex, ulong>>
    class EdgeTrie {
    public:
        EdgeTrie() {
            _data.emplace_back();
            _transitions.emplace_back();
        }

        Data& operator[](const Edge& edge) {
            ulong index = 0;
            for (const Vertex vertex : edge) {
                const auto transition = _transitions[index].find(vertex);
                if (transition == _transitions[index].end()) {
                    index = _transitions[index][vertex] = CreateNewEntryAndGetIndex();
                } else {
                    index = transition->second;
                }
            }

            return _data[index];
        }

    private:
        ulong CreateNewEntryAndGetIndex() {
            const ulong index = _data.size();
            _data.emplace_back();
            _transitions.emplace_back();
            return index;
        }

    private:
        std::vector<Data> _data;
        std::vector<TransitionsMap> _transitions;
    };
} // namespace hittingset
