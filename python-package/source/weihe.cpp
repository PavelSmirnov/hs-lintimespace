#include "helpers.h"
#include "weihe.h"
#include <algorithm>
#include <numeric>
#include <unordered_set>


namespace hittingset {
    namespace {
        uint GetNumVertices(const Hypergraph& instance) {
            uint num_vertices = 0;
            for (const auto& edge : instance) {
                for (const auto vertex : edge) {
                    num_vertices = std::max<uint>(num_vertices, vertex + 1);
                }
            }
            return num_vertices;
        }

        Hypergraph GetDual(const Hypergraph& instance) {
            uint num_vertices = GetNumVertices(instance);
            Hypergraph dual(num_vertices);
            for (uint edge_id = 0; edge_id < instance.size(); ++edge_id) {
                for (const auto vertex : instance[edge_id]) {
                    dual[vertex].push_back(edge_id);
                }
            }
            return dual;
        }

        bool IsSubedge(const Edge& subedge, const Edge& edge) {
            uint edge_pos = 0;
            for (uint subedge_pos = 0; subedge_pos < subedge.size(); ++subedge_pos) {
                while (edge_pos < edge.size() && edge[edge_pos] != subedge[subedge_pos]) {
                    ++edge_pos;
                }
                if (edge_pos == edge.size()) {
                    return false;
                }
            }
            return true;
        }

        Hypergraph ReduceVertices(const Hypergraph& instance, std::ostream* logger, bool* vertices_reduced) {
            Hypergraph dual = SortEdgesVertices(GetDual(instance));

            std::vector<uint> permutation(dual.size());
            std::iota(permutation.begin(), permutation.end(), 0);
            std::sort(permutation.begin(), permutation.end(), [&](const uint lhs, const uint rhs) {
                return std::make_pair(dual[lhs].size(), lhs) > std::make_pair(dual[rhs].size(), rhs);
            });

            std::unordered_set<uint> skip_vertices;
            while (permutation.size() > 0) {
                const uint vertex = permutation.back();
                permutation.pop_back();
                const auto vertex_edges = dual[vertex];
                for (const auto super_vertex : permutation) {
                    if (IsSubedge(vertex_edges, dual[super_vertex])) {
                        skip_vertices.insert(vertex);
                        break;
                    }
                }
            }
            *vertices_reduced = !skip_vertices.empty();
            (*logger) << "Removed vertices num: " << skip_vertices.size() << std::endl;

            Hypergraph reduced;
            for (const auto& edge : instance) {
                reduced.emplace_back();
                auto& new_edge = reduced.back();
                for (const auto vertex : edge) {
                    if (skip_vertices.find(vertex) == skip_vertices.end()) {
                        new_edge.push_back(vertex);
                    }
                }
            }
            return reduced;
        }

        Hypergraph ReduceEdgesIteratingEdges(const Hypergraph& sorted_instance, bool* edges_reduced) {
            Hypergraph reduced;
            for (const auto& edge : sorted_instance) {
                bool has_subedge = false;
                for (const auto& maybe_subedge : reduced) {
                    if (IsSubedge(maybe_subedge, edge)) {
                        has_subedge = true;
                        break;
                    }
                }
                if (!has_subedge) {
                    reduced.push_back(edge);
                }
            }
            *edges_reduced = reduced.size() < sorted_instance.size();
            return reduced;
        }

        Hypergraph ReduceEdgesIteratingSubsets(const Hypergraph& sorted_instance, bool* edges_reduced) {
            Hypergraph reduced;
            std::unordered_set<Edge, EdgeHasher> used_edges;
            for (const auto& edge : sorted_instance) {
                bool has_subedge = false;
                for (const auto& subedge : GetAllSubsets(edge)) {
                    if (used_edges.find(subedge) != used_edges.end()) {
                        has_subedge = true;
                        break;
                    }
                }
                if (!has_subedge) {
                    reduced.push_back(edge);
                    used_edges.insert(edge);
                }
            }
            *edges_reduced = reduced.size() < sorted_instance.size();
            return reduced;
        }

        Hypergraph ReduceEdges(const Hypergraph& instance, std::ostream* logger, bool* edges_reduced) {
            Hypergraph sorted = SortEdgesBySize(instance);

            uint max_edge_size;
            uint max_vertex;
            CalcMaxEdgeSizeAndMaxVertex(sorted, &max_edge_size, &max_vertex);

            if (max_edge_size >= 64 || instance.size() <= (1ULL << max_edge_size)) {
                return ReduceEdgesIteratingEdges(sorted, edges_reduced);
            } else {
                return ReduceEdgesIteratingSubsets(sorted, edges_reduced);
            }
        }
    } // namespace anonymous

    void WeiheReducer::Reduce(const Hypergraph& instance, Hypergraph* result, std::ostream* logger) const {
        *result = SortEdgesVertices(instance);
        (*logger) << "Initial size: " << result->size() << std::endl;

        bool first_iteration = true;
        do {
            bool edges_reduced;
            *result = ReduceEdges(*result, logger, &edges_reduced);
            (*logger) << "Size after edges reduction: " << result->size() << std::endl;
            if (!edges_reduced && !first_iteration) {
              break;
            }

            bool vertices_reduced;
            *result = ReduceVertices(*result, logger, &vertices_reduced);
            (*logger) << "Size after vertices reduction: " << result->size() << std::endl;
            if (!vertices_reduced) {
              break;
            }

            first_iteration = false;
        } while (true);
    }
} // namespace hittingset
