#pragma once
#include "reducer.h"
#include <sstream>

namespace hittingset {
    class FafianieKratschReducer : public MaxHSParameterizedReducer {
    public:
        using MaxHSParameterizedReducer::Reduce;
    protected:
        void Reduce(const Hypergraph& instance,
                    const uint max_hitting_set,
                    const uint max_all_subsets_edge,
                    Hypergraph* result,
                    std::ostream* logger) const override;
    };
} // namespace hittingset
