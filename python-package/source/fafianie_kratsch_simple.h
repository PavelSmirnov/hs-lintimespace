#pragma once
#include "helpers.h"
#include "reducer.h"
#include <map>
#include <sstream>
#include <unordered_map>
#include <vector>

namespace hittingset {
    template <class EdgeIndexedMap>
    class FafianieKratschSimpleReducer : public MaxHSParameterizedReducer {
    public:
        using MaxHSParameterizedReducer::Reduce;
    protected:
        void Reduce(const Hypergraph& instance,
                    const uint max_hitting_set,
                    const uint max_all_subsets_edge,
                    Hypergraph* kernel_ptr,
                    std::ostream* logger_ptr) const override {
            assert(kernel_ptr);
            assert(logger_ptr);
            assert(max_all_subsets_edge < 33);

            *logger_ptr << "number of edges = " << instance.size() << std::endl;

            auto sorted_instance = SortEdgesVertices(instance);
            sorted_instance = SortEdgesBySize(sorted_instance);

            uint max_edge_size;
            uint max_vertex;
            CalcMaxEdgeSizeAndMaxVertex(sorted_instance, &max_edge_size, &max_vertex);

            const auto superset_bounds = CalcFKSupersetBounds(max_hitting_set,
                                                              max_edge_size,
                                                              sorted_instance.size());
            *logger_ptr << "kernel upper bound = " << superset_bounds[0] << std::endl;

            EdgeIndexedMap num_supersets;
            for (const auto& edge : sorted_instance) {
                const std::vector<Edge> subsets = GetSubsets(edge, instance, max_all_subsets_edge, max_vertex);

                bool take = true;
                for (const auto& subset : subsets) {
                    if (num_supersets[subset] >= superset_bounds[subset.size()]) {
                      take = false;
                      break;
                    }
                }
                if (!take) {
                  continue;
                }

                kernel_ptr->push_back(edge);

                for (const auto& subset : subsets) {
                    if (edge.size() == subset.size()) {
                      num_supersets[subset] = superset_bounds[subset.size()]; // to remove all supersets
                      continue;
                    }
                    ++num_supersets[subset];
                }
            }
            *logger_ptr << "kernel size = " << kernel_ptr->size() << std::endl;
        }
    };

    using FafianieKratschHashingReducer =
        FafianieKratschSimpleReducer<std::unordered_map<Edge, ulong, EdgeHasher>>;

    using FafianieKratschMappingReducer =
        FafianieKratschSimpleReducer<std::map<Edge, ulong, EdgeComparator>>;

    using FafianieKratschWithTrieReducer = FafianieKratschSimpleReducer<EdgeTrie<ulong>>;

    using FafianieKratschWithMapTrieReducer =
        FafianieKratschSimpleReducer<EdgeTrie<ulong, std::map<Vertex, ulong>>>;
} // namespace hittingset
