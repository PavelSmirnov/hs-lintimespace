import os
from setuptools import find_packages, setup
from setuptools.extension import Extension

from Cython.Build import cythonize

REQUIREMENTS = [
    "Cython>=0.29.21",
    "numpy>=1.17.4",
    "pytest>=5.3.1",
    "setuptools>=49.2.0",
]

cython_ext = Extension('hs_lintimespace.clib.clib',
                       [os.path.join('hs_lintimespace', 'clib', 'clib.pyx')],
                       include_dirs=['source'],
                       language='c++',
                       extra_compile_args=['-std=c++17', '-O2'])

setup(name='hs_lintimespace',
      version='0.1',
      author='Pavel Smirnov',
      description='d-Hitting Set kernelization algorithms with the solution bound as a parameter.',
      url='https://gitlab.com/PavelSmirnov/hs-lintimespace',
      packages=find_packages(),
      ext_modules=cythonize([cython_ext], compiler_directives={'language_level': 3}),
      install_requires=REQUIREMENTS)
