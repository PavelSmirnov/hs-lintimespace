from itertools import combinations

import numpy as np
import pytest

from hs_lintimespace.dataset.instance import Instance
from hs_lintimespace.reducer.bound_hs import (FafianieKratschReducer, VanBevernReducer,
                                              FafianieKratschHashingReducer, VanBevernHashingReducer,
                                              FafianieKratschMappingReducer, VanBevernMappingReducer,
                                              FafianieKratschWithTrieReducer, VanBevernWithTrieReducer,
                                              FafianieKratschWithMapTrieReducer, VanBevernWithMapTrieReducer)
from hs_lintimespace.reducer.helpers import bruteforce_hitting_sets
from hs_lintimespace.reducer.standard import UniqueReducer


@pytest.mark.parametrize('reducer', [FafianieKratschReducer(), VanBevernReducer(),
                                     FafianieKratschHashingReducer(), VanBevernHashingReducer(),
                                     FafianieKratschMappingReducer(), VanBevernMappingReducer(),
                                     FafianieKratschWithTrieReducer(), VanBevernWithTrieReducer(),
                                     FafianieKratschWithMapTrieReducer(), VanBevernWithMapTrieReducer()])
def test_empty(reducer):
    instance = Instance([])
    kernel, _ = reducer(instance)
    assert kernel == Instance([])


@pytest.mark.parametrize('reducer', [FafianieKratschReducer(), VanBevernReducer(),
                                     FafianieKratschHashingReducer(), VanBevernHashingReducer(),
                                     FafianieKratschMappingReducer(), VanBevernMappingReducer(),
                                     FafianieKratschWithTrieReducer(), VanBevernWithTrieReducer(),
                                     FafianieKratschWithMapTrieReducer(), VanBevernWithMapTrieReducer()])
def test_1_hitting_set(reducer):
    np.random.seed(0)
    for size in range(10):
        instance = Instance(list(map(tuple, np.random.randint(0, size, size=size)[:, np.newaxis])))
        reduced, _ = reducer(instance)
        expected, _ = UniqueReducer()(instance)
        assert reduced == expected


INPUTS = [
    (1, [(0, 1), (0, 2), (0, 3)]),
    (1, [(0, 1, 2), (0, 1, 3), (0, 1, 4)]),
    (1, [(0, 1, 2, 3), (0, 1, 4, 5), (0, 1, 6, 7), (0, 1, 8, 9), (0, 1, 10, 11)]),
    (1, [(0, 1, 2, 3), (0, 1, 3, 4), (0, 1, 4, 5), (0, 1, 5, 6), (0, 1, 6, 7)]),
    (2, [(0, 1, 2, 3), (2, 3, 4, 5), (4, 5, 6, 7), (6, 7, 8, 9), (0, 2, 3, 4), (1, 2, 3, 4), (4, 6, 7, 8), (5, 6, 7, 8), (0, 2, 3, 5), (1, 2, 3, 5), (4, 6, 7, 9), (5, 6 ,7, 9)]),
    (2, [(0, 1, 2, 3), (0, 1, 3, 4), (0, 1, 4, 5), (0, 1, 5, 6), (0, 1, 6, 7), (0, 1, 7, 8), (0, 1, 8, 9), (0, 1, 9, 10), (0, 1, 10, 11), (0, 1, 11, 12), (0, 1, 12, 13)]),
    (1, [(0, 2, 3), (0, 4, 5), (0, 6, 7), (0, 8, 9), (0, 1)]),
]

BEV_OUTPUTS = [
    [(0, 1), (0, 2)],
    [(0, 1, 2), (0, 1, 3)],
    [(0, 1, 2, 3), (0, 1, 4, 5)],
    [(0, 1, 2, 3), (0, 1, 3, 4), (0, 1, 4, 5)],
    [(0, 1, 2, 3), (2, 3, 4, 5), (4, 5, 6, 7), (6, 7, 8, 9), (0, 2, 3, 4), (1, 2, 3, 4), (4, 6, 7, 8), (5, 6, 7, 8), (0, 2, 3, 5), (1, 2, 3, 5), (4, 6, 7, 9), (5, 6 ,7, 9)],
    [(0, 1, 2, 3), (0, 1, 3, 4), (0, 1, 4, 5), (0, 1, 5, 6), (0, 1, 6, 7)],
    [(0, 1), (0, 2, 3)],
]

FK_OUTPUTS = [
    [(0, 1), (0, 2)],
    [(0, 1, 2), (0, 1, 3)],
    [(0, 1, 2, 3), (0, 1, 4, 5), (0, 1, 6, 7), (0, 1, 8, 9)],
    [(0, 1, 2, 3), (0, 1, 3, 4), (0, 1, 4, 5), (0, 1, 5, 6)],
    [(0, 1, 2, 3), (2, 3, 4, 5), (4, 5, 6, 7), (6, 7, 8, 9), (0, 2, 3, 4), (1, 2, 3, 4), (4, 6, 7, 8), (5, 6, 7, 8), (0, 2, 3, 5), (1, 2, 3, 5), (4, 6, 7, 9), (5, 6 ,7, 9)],
    [(0, 1, 2, 3), (0, 1, 3, 4), (0, 1, 4, 5), (0, 1, 5, 6), (0, 1, 6, 7), (0, 1, 7, 8), (0, 1, 8, 9), (0, 1, 9, 10), (0, 1, 10, 11)],
    [(0, 1), (0, 2, 3), (0, 4, 5), (0, 6, 7)],
]

@pytest.mark.parametrize('reducer_cls, inputs, outputs', [
    (VanBevernReducer, INPUTS, BEV_OUTPUTS),
    (VanBevernHashingReducer, INPUTS, BEV_OUTPUTS),
    (VanBevernMappingReducer, INPUTS, BEV_OUTPUTS),
    (VanBevernWithTrieReducer, INPUTS, BEV_OUTPUTS),
    (VanBevernWithMapTrieReducer, INPUTS, BEV_OUTPUTS),
    (FafianieKratschReducer, INPUTS, FK_OUTPUTS),
    (FafianieKratschHashingReducer, INPUTS, FK_OUTPUTS),
    (FafianieKratschMappingReducer, INPUTS, FK_OUTPUTS),
    (FafianieKratschWithTrieReducer, INPUTS, FK_OUTPUTS),
    (FafianieKratschWithMapTrieReducer, INPUTS, FK_OUTPUTS),
])
def test_handcrafted(reducer_cls, inputs, outputs):
    max_hitting_sets, instances = zip(*inputs)
    for instance, max_hitting_set, expected in zip(instances, max_hitting_sets, outputs):
        instance, expected = map(Instance, (instance, expected))
        print(instance)
        reducer = reducer_cls(max_hitting_set=max_hitting_set)
        reduced, _ = reducer(instance)
        assert reduced == expected


@pytest.mark.parametrize('reducer_cls', [FafianieKratschReducer,
                                         FafianieKratschHashingReducer,
                                         FafianieKratschMappingReducer,
                                         FafianieKratschWithTrieReducer,
                                         FafianieKratschWithMapTrieReducer])
@pytest.mark.parametrize('num_edges', [10 ** i for i in range(1, 5)])
@pytest.mark.parametrize('num_vertices', [10 * i for i in range(1, 11, 2)])
def test_fk_max_size(reducer_cls, num_edges, num_vertices):
    np.random.seed(0)
    for k in range(1, 4):
        for d in range(1, 5):
            edges = []
            for i in range(num_edges):
                edge = set()
                while len(edge) < d:
                    edge.add(np.random.randint(num_vertices))
                edges.append(tuple(edge))
            reduced, _ = reducer_cls(max_hitting_set=k)(Instance(edges))
            assert len(reduced) <= (k + 1) ** d


@pytest.mark.parametrize('reducer_cls', [FafianieKratschReducer, VanBevernReducer,
                                         FafianieKratschHashingReducer, VanBevernHashingReducer,
                                         FafianieKratschMappingReducer, VanBevernMappingReducer,
                                         FafianieKratschWithTrieReducer, VanBevernWithTrieReducer,
                                         FafianieKratschWithMapTrieReducer, VanBevernWithMapTrieReducer])
@pytest.mark.parametrize('num_vertices, edge_size', [(4, 2), (5, 3)])
def test_kernel_correctness(reducer_cls, num_vertices, edge_size):
    all_edges = list(combinations(np.arange(num_vertices), edge_size))
    for num_edges in range(len(all_edges) + 1):
        for edges in combinations(all_edges, num_edges):
            instance = Instance(list(edges))
            instance_hitting_sets = bruteforce_hitting_sets(instance)
            hs_upperbound = min(map(len, instance_hitting_sets))

            reducer = reducer_cls(max_hitting_set=hs_upperbound)
            kernel, _ = reducer(instance)
            kernel_hitting_sets = bruteforce_hitting_sets(kernel)

            def cut_by_upperbound(hitting_sets):
                return set(filter(lambda hs: len(hs) <= hs_upperbound, hitting_sets))

            instance_hitting_sets = cut_by_upperbound(instance_hitting_sets)
            kernel_hitting_sets = cut_by_upperbound(kernel_hitting_sets)
            assert instance_hitting_sets == kernel_hitting_sets


@pytest.mark.parametrize('reducer_cls', [FafianieKratschReducer,
                                         FafianieKratschHashingReducer,
                                         FafianieKratschMappingReducer,
                                         FafianieKratschWithTrieReducer,
                                         FafianieKratschWithMapTrieReducer])
def test_fk_max_size_disjoint_edges(reducer_cls):
    for max_hitting_set in range(1, 10):
        for max_edge_size in range(2, 5):
            reducer = reducer_cls(max_hitting_set)
            max_kernel_size = (max_hitting_set + 1) ** max_edge_size
            num_edges = max_kernel_size * 2
            vertices = np.arange(num_edges * max_edge_size)
            edges = []
            for i in range(0, len(vertices), max_edge_size):
                edges.append(tuple(vertices[i:i + max_edge_size]))
            instance = Instance(edges)
            kernel, _ = reducer(instance)
            assert len(kernel) <= max_kernel_size
