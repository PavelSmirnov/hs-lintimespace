from libcpp.string cimport string
from libcpp.vector cimport vector

ctypedef unsigned int Vertex

cdef extern from "helpers.cpp" namespace "hittingset":
    pass

cdef extern from "van_bevern.cpp" namespace "hittingset":
    pass

cdef extern from "fafianie_kratsch.cpp" namespace "hittingset":
    pass

cdef extern from "weihe.cpp" namespace "hittingset":
    pass

cdef extern from "reducer.h" namespace "hittingset":
    cdef cppclass ReductionResult:
        vector[vector[Vertex]] instance
        double time_in_sec
        string log

cdef extern from "van_bevern.h" namespace "hittingset":
    cdef cppclass VanBevernReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance,
                               const Vertex max_hitting_set,
                               const Vertex max_all_subsets_edge) except +

cdef extern from "fafianie_kratsch.h" namespace "hittingset":
    cdef cppclass FafianieKratschReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance,
                               const Vertex max_hitting_set,
                               const Vertex max_all_subsets_edge) except +

cdef extern from "weihe.h" namespace "hittingset":
    cdef cppclass WeiheReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance) except +

cdef extern from "van_bevern_simple.h" namespace "hittingset":
    cdef cppclass VanBevernHashingReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance,
                               const Vertex max_hitting_set,
                               const Vertex max_all_subsets_edge) except +

    cdef cppclass VanBevernMappingReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance,
                               const Vertex max_hitting_set,
                               const Vertex max_all_subsets_edge) except +

    cdef cppclass VanBevernWithTrieReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance,
                               const Vertex max_hitting_set,
                               const Vertex max_all_subsets_edge) except +

    cdef cppclass VanBevernWithMapTrieReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance,
                               const Vertex max_hitting_set,
                               const Vertex max_all_subsets_edge) except +

cdef extern from "fafianie_kratsch_simple.h" namespace "hittingset":
    cdef cppclass FafianieKratschHashingReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance,
                               const Vertex max_hitting_set,
                               const Vertex max_all_subsets_edge) except +

    cdef cppclass FafianieKratschMappingReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance,
                               const Vertex max_hitting_set,
                               const Vertex max_all_subsets_edge) except +

    cdef cppclass FafianieKratschWithTrieReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance,
                               const Vertex max_hitting_set,
                               const Vertex max_all_subsets_edge) except +

    cdef cppclass FafianieKratschWithMapTrieReducer:
        ReductionResult Reduce(const vector[vector[Vertex]]& instance,
                               const Vertex max_hitting_set,
                               const Vertex max_all_subsets_edge) except +
