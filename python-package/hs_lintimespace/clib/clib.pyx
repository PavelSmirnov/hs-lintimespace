from hs_lintimespace.clib.clib cimport (Vertex, ReductionResult,
                                        VanBevernReducer, FafianieKratschReducer, WeiheReducer,
                                        VanBevernHashingReducer, FafianieKratschHashingReducer,
                                        VanBevernMappingReducer, FafianieKratschMappingReducer,
                                        VanBevernWithTrieReducer, FafianieKratschWithTrieReducer,
                                        VanBevernWithMapTrieReducer, FafianieKratschWithMapTrieReducer)
from libcpp cimport vector

cdef _convert_result(ReductionResult result):
    reduced = [tuple(vertex for vertex in edge) for edge in result.instance]
    time = result.time_in_sec
    log = result.log.decode('utf-8')
    return reduced, {"time": time, "log": log}

cdef class CVanBevernReducer(object):
    cdef VanBevernReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges, max_hitting_set, max_all_subsets_edge):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges, max_hitting_set, max_all_subsets_edge)
        return _convert_result(result)

cdef class CFafianieKratschReducer(object):
    cdef FafianieKratschReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges, max_hitting_set, max_all_subsets_edge):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges, max_hitting_set, max_all_subsets_edge)
        return _convert_result(result)

cdef class CWeiheReducer(object):
    cdef WeiheReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges)
        return _convert_result(result)

cdef class CVanBevernHashingReducer(object):
    cdef VanBevernHashingReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges, max_hitting_set, max_all_subsets_edge):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges, max_hitting_set, max_all_subsets_edge)
        return _convert_result(result)

cdef class CFafianieKratschHashingReducer(object):
    cdef FafianieKratschHashingReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges, max_hitting_set, max_all_subsets_edge):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges, max_hitting_set, max_all_subsets_edge)
        return _convert_result(result)

cdef class CVanBevernMappingReducer(object):
    cdef VanBevernMappingReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges, max_hitting_set, max_all_subsets_edge):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges, max_hitting_set, max_all_subsets_edge)
        return _convert_result(result)

cdef class CFafianieKratschMappingReducer(object):
    cdef FafianieKratschMappingReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges, max_hitting_set, max_all_subsets_edge):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges, max_hitting_set, max_all_subsets_edge)
        return _convert_result(result)

cdef class CVanBevernWithTrieReducer(object):
    cdef VanBevernWithTrieReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges, max_hitting_set, max_all_subsets_edge):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges, max_hitting_set, max_all_subsets_edge)
        return _convert_result(result)

cdef class CFafianieKratschWithTrieReducer(object):
    cdef FafianieKratschWithTrieReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges, max_hitting_set, max_all_subsets_edge):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges, max_hitting_set, max_all_subsets_edge)
        return _convert_result(result)

cdef class CVanBevernWithMapTrieReducer(object):
    cdef VanBevernWithMapTrieReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges, max_hitting_set, max_all_subsets_edge):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges, max_hitting_set, max_all_subsets_edge)
        return _convert_result(result)

cdef class CFafianieKratschWithMapTrieReducer(object):
    cdef FafianieKratschWithMapTrieReducer _reducer

    def __cinit__(self):
        pass

    def __call__(self, edges, max_hitting_set, max_all_subsets_edge):
        cdef vector[vector[Vertex]] c_edges = edges
        cdef ReductionResult result = self._reducer.Reduce(c_edges, max_hitting_set, max_all_subsets_edge)
        return _convert_result(result)
