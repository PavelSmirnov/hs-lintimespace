import bz2
import lzma
import os

from hs_lintimespace.dataset.base import BaseDataset
from hs_lintimespace.dataset.instance import Instance
from hs_lintimespace.dataset.helpers import find_3paths


class CustomDataset(BaseDataset):
    """
    A dataset compiled from arbitrary instances.
    """

    def __init__(self, instances, **kwargs):
        """
        Parameters:
        -----------
        instances: Iterable of Instance
            instances of the dataset
        """
        super(CustomDataset, self).__init__(**kwargs)

        self._instances = [instance for instance in instances]
        self._index = 0

    def __next__(self):
        if self._index >= len(self._instances):
            raise StopIteration
        instance = self._instances[self._index]
        self._index += 1
        return instance

    def save(self, dir_name):
        if not os.path.isdir(dir_name):
            os.mkdir(dir_name)

        for index, instance in enumerate(self._instances):
            instance.save(os.path.join(dir_name, str(index) + '.hyp'))

    @property
    def instances(self):
        return self._instances


class FilesDataset(BaseDataset):
    """
    Loads graphs from files to assemble a dataset.
    """

    def __init__(self, dir_name, **kwargs):
        """
        Parameters:
        -----------
        dir_name: str
            path to a directory with hypergraphs
            all '.hyp' files inside will be assembled into a dataset
        """
        super(FilesDataset, self).__init__(**kwargs)

        assert os.path.isdir(dir_name), f"No such directory {dir_name}."
        self._dir_name = dir_name
        self._files = [name for name in os.listdir(self._dir_name) if name.endswith('.hyp')]

    def __next__(self):
        if not self._files:
            raise StopIteration
        path = os.path.join(self._dir_name, self._files.pop())
        return Instance(path)


class BioDataset(BaseDataset):
    """
    Cuts edges in a complete weighted graph by threshold and converts into Cluster Vertex Deletion instance.
    """

    def __init__(self, dir_name, max_vertices=None, threshold=0.0, **kwargs):
        """
        Parameters:
        -----------
        dir_name: str
            path to a directory with hypergraphs
            all '.cm' files inside will be assembled into a dataset

        max_vertices: int
            max number of vertices, any if None

        threshold: float
            edge weight threshold, all edges with lower threshold will be cut
        """
        super(BioDataset, self).__init__(**kwargs)

        self._threshold = threshold

        assert os.path.isdir(dir_name), f"No such directory {dir_name}."
        self._dir_name = dir_name
        self._max_vertices = max_vertices

        self._files = [name for name in os.listdir(self._dir_name) if name.endswith('.cm')]
        if max_vertices is not None:
            self._files = [name for name in self._files if self._get_num_vertices(name) <= max_vertices]
        self._files = sorted(self._files, key=self._get_num_vertices)

    def _get_num_vertices(self, path):
        tokens = path.split('_')
        return int(tokens[tokens.index('size') + 1])

    def __next__(self):
        if not self._files:
            raise StopIteration
        path = os.path.join(self._dir_name, self._files.pop())
        return self._create_instance(path)

    def _create_instance(self, path):
        with open(path) as f:
            lines = [line for line in f]
        num_vertices = int(lines[0])
        matrix = [list(map(float, line.split())) for line in lines[num_vertices + 1:]]
        graph = [set() for _ in range(num_vertices)]
        for i in range(num_vertices):
            for j in range(i + 1, num_vertices):
                if matrix[i][j - i - 1] >= self._threshold:
                    graph[i].add(j)
                    graph[j].add(i)
        return Instance(find_3paths(graph), {'threshold': self._threshold})


class SATLIBDataset(BaseDataset):
    """
    CNF satisfiability problem instances in SATLIB format.
    https://www.cs.ubc.ca/~hoos/SATLIB/benchm.html
    """

    def __init__(self, dir_name, **kwargs):
        """
        Parameters:
        -----------
        dir_name: str
            path to a directory with problem instances
            all '.cnf' files inside will be assembled into a dataset
        """
        super(SATLIBDataset, self).__init__(**kwargs)

        assert os.path.isdir(dir_name), f"No such directory {dir_name}."
        self._dir_name = dir_name

        self._files = [name for name in os.listdir(self._dir_name) if name.endswith('.cnf') or name.endswith('.cnf.xz') or name.endswith('.cnf.bz2')]
        self._files = list(reversed(self._files))

    def __next__(self):
        if not self._files:
            raise StopIteration
        path = os.path.join(self._dir_name, self._files.pop())
        if path.endswith('.xz'):
            with lzma.open(path, 'rt') as f:
                return self._create_instance(f)
        if path.endswith('.bz2'):
            with bz2.open(path, 'rt') as f:
                return self._create_instance(f)
        with open(path) as f:
            return self._create_instance(f)

    def _create_instance(self, file):
        lines = [line.strip() for line in file]
        # remove comments
        lines = [line for line in lines if not line.startswith('c')]

        assert len(lines) > 0 and lines[0].startswith('p'), 'First non-comment line must specify problem type.'
        problem_type = lines[0].split()
        assert len(problem_type) == 4 and problem_type[0] == 'p' and problem_type[1] == 'cnf', 'Invalid problem type.'

        num_variables = int(problem_type[2])
        num_clauses = int(problem_type[3])

        def get_vertex(literal):
            if literal < 0:
                return -literal - 1 + num_variables
            return literal - 1

        edges = []
        for variable in range(1, num_variables + 1):
            edges.append((get_vertex(variable), get_vertex(-variable)))

        clauses = [[]]
        for line in lines[1:]:
            if line.startswith('%'):
                break
            literals = list(map(int, line.split()))
            for literal in literals:
                if literal == 0:
                    clauses.append([])
                    continue
                clauses[-1].append(get_vertex(literal))
        if len(clauses[-1]) == 0:
            clauses.pop()
        assert len(clauses) == num_clauses, 'Bad file reading.'

        edges += list(map(tuple, clauses))

        return Instance(edges)
