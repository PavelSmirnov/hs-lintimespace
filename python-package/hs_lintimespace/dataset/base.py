import os

class BaseDataset(object):
    """
    A dataset with hypergraphs.

    Use in a for loop like this:
    > for instance in BaseDatasetSuccessor(**kwargs):
    >     solve(instance)
    """

    def __init__(self):
        pass

    def __iter__(self):
        return self

    def __next__(self):
        raise NotImplementedError

    def save(self, dir_name, name_prefix):
        assert os.path.isdir(dir_name), f"No such directory: {dir_name}."

        for num, instance in enumerate(self):
            instance.save(os.path.join(dir_name, name_prefix + str(num)))
