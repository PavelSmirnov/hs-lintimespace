import copy
import random

import numpy as np

from hs_lintimespace.dataset.base import BaseDataset
from hs_lintimespace.dataset.instance import Instance
from hs_lintimespace.dataset.helpers import find_3paths, get_unique_edges


class GolombDataset(BaseDataset):
    """
    Generates Golomb ruler problem instances.
    """

    def __init__(self, ruler_lens, **kwargs):
        """
        Parameters:
        -----------
        ruler_lens: list of int
            lengths of Golomb rulers, bound the number of vertices
        """
        super(GolombDataset, self).__init__(**kwargs)

        self._ruler_lens = ruler_lens
        self._current_instance = 0

    def __next__(self):
        if self._current_instance >= len(self._ruler_lens):
            raise StopIteration
        self._current_instance += 1
        return Instance(self._generate_edges(self._ruler_lens[self._current_instance - 1]))

    def _generate_edges(self, ruler_len):
        edges = []
        for diff in range(1, ruler_len + 1):
            for r in range(diff, ruler_len + 1):
                for l in range(diff, r):
                    edges.append(tuple({l - diff, l, r - diff, r}))
        return get_unique_edges(edges)


class GIRGDataset(BaseDataset):
    """
    Generates instances according to https://arxiv.org/abs/1905.12477

    Use in a for loop like this:
    > for instance in GIRGDataset(**kwargs):
    >     solve(instance)
    """

    def __init__(self, num_instances, num_vertices, num_edges, skip_empty=True,
                 alpha=4e-4, beta=3.5, temperature=0.5, seed=0, **kwargs):
        """
        Parameters:
        -----------
        num_instances: int
            number of instances to generate

        num_vertices: int
            number of vertices in each instance

        num_edges: int
            number of hyperedges in each instance

        skip_empty: bool
            whether to skip empty edges

        alpha: float
            affects average vertex degree

        beta: float
            affects heterogenity (normal value is around [2, 6] segment)

        temperature: float
            affects locality

        seed: int (32-bit unsigned)
            random seed
        """
        super(GIRGDataset, self).__init__(**kwargs)

        self._num_instances = num_instances
        self._num_vertices = num_vertices
        self._num_edges = num_edges
        self._skip_empty = skip_empty
        self._alpha = alpha
        self._beta = beta
        self._temperature = temperature
        self._seed = seed

    def __next__(self):
        if self._num_instances <= 0:
            raise StopIteration
        self._num_instances -= 1
        np.random.seed(self._seed)
        edges = self._generate_edges()
        self._seed = np.random.randint(1 << 32)
        return Instance(edges)

    def _generate_edges(self):
        vertex_weights = np.random.uniform(size=self._num_vertices)
        edge_weights = self._beta * np.random.exponential(scale=self._beta, size=self._num_edges)

        vertex_positions = np.random.uniform(size=self._num_vertices)
        edge_positions = np.random.uniform(size=self._num_edges)

        edges = []
        for edge_weight, edge_position in zip(edge_weights, edge_positions):
            are_connected = lambda vertex: self._are_connected(edge_weight,
                                                               edge_position,
                                                               vertex_weights[vertex],
                                                               vertex_positions[vertex])
            edge = tuple(filter(are_connected, range(self._num_vertices)))
            if edge or not self._skip_empty:
                edges.append(edge)
        return edges

    def _are_connected(self, edge_weight, edge_position, vertex_weight, vertex_position):
        border = self._alpha * edge_weight * vertex_weight / abs(edge_position - vertex_position)
        prob = border ** (1.0 / self._temperature)
        if prob >= 1.0:
            return True
        return np.random.random() <= prob


class ClusterVertexDeletionDataset(BaseDataset):
    """
    Generates Cluster Vertex Deletion problem instances which is
    to remove the smallest set of vertices s.t. each connected component is a clique.
    (The hyperedges for Hitting Set problem are the 3-paths induced subgraphs.)
    """

    def __init__(self, num_instances, graph_size=100, num_cliques=5,
                 strategy='duplicate_vertex', extra=10, seed=0, **kwargs):
        """
        Parameters:
        -----------
        num_instances: int
            the number if instances

        graph_size: int
            the number of true vertices in each graph

        num_cliques: int
            the number of cliques in each graph

        strategy: str
            one of 'duplicate_vertex', 'add_edge', 'delete_edge', 'flip_edge'

        extra: int
            the number of strategy moves

        seed: int
            random seed
        """
        super(ClusterVertexDeletionDataset, self).__init__(**kwargs)

        assert graph_size >= num_cliques, 'The number of cliques must be at least the size of a graph'
        self._num_instances = num_instances
        self._graph_size = graph_size
        self._num_cliques = num_cliques
        self._strategy = strategy
        self._extra = extra
        self._seed = seed

    def __next__(self):
        if self._num_instances == 0:
            raise StopIteration
        self._num_instances -= 1
        np.random.seed(self._seed)
        instance = Instance(self._generate_edges())
        self._seed = np.random.randint(1 << 32)
        return instance

    def _generate_edges(self):
        graph = self._generate_graph()
        return find_3paths(graph)

    def _generate_graph(self):
        clique_num = np.random.randint(self._num_cliques, size=self._graph_size)
        clique_num[:self._num_cliques] = np.arange(self._num_cliques, dtype=int)
        np.random.shuffle(clique_num)
        graph = []
        for vertex in range(self._graph_size):
            graph.append(set())
            for neighbor in filter(lambda x: clique_num[x] == clique_num[vertex], range(vertex)):
                graph[neighbor].add(vertex)
                graph[vertex].add(neighbor)
        self._add_extra(graph)
        return graph

    def _add_extra(self, graph):
        if self._strategy == 'duplicate_vertex':
            self._duplicate_vertex(graph)
        elif self._strategy == 'add_edge':
            self._add_edge(graph)
        elif self._strategy == 'delete_edge':
            self._delete_edge(graph)
        elif self._strategy == 'flip_edge':
            self._flip_edge(graph)
        else:
            assert False, f"Unknown strategy: {self._strategy}."

    def _duplicate_vertex(self, graph):
        while len(graph) < self._graph_size + self._extra:
            vertex = np.random.randint(self._graph_size)
            graph.append(copy.copy(graph[vertex]))
            for neighbor in graph[-1]:
                graph[neighbor].add(len(graph) - 1)

    def _add_edge(self, graph):
        edges = []
        for v in range(self._graph_size):
            for u in range(v):
                if u not in graph[v]:
                    edges.append((u, v))
        np.random.shuffle(edges)
        for u, v in edges[:self._extra]:
            graph[u].add(v)
            graph[v].add(u)

    def _delete_edge(self, graph):
        edges = []
        for v in range(self._graph_size):
            for u in range(v):
                if u in graph[v]:
                    edges.append((u, v))
        np.random.shuffle(edges)
        for u, v in edges[:self._extra]:
            graph[u].remove(v)
            graph[v].remove(u)

    def _flip_edge(self, graph):
        edges = []
        for v in range(self._graph_size):
            for u in range(v):
                edges.append((u, v))
        np.random.shuffle(edges)
        for u, v in edges[:self._extra]:
            if u in graph[v]:
                graph[u].remove(v)
                graph[v].remove(u)
            else:
                graph[u].add(v)
                graph[v].add(u)


class UniformDataset(BaseDataset):
    """
    Generates d-uniform hypergraphs.
    """

    def __init__(self, num_instances, num_vertices, num_edges, edge_size, seed=0, **kwargs):
        """
        Parameters:
        -----------
        num_instances: int
            the number if instances

        num_vertices: int
            the number of vertices in each graph

        num_edges: int
            the number of edges in each graph

        edge_size: int
            the number of vertices in each edge

        seed: int
            random seed
        """
        super(UniformDataset, self).__init__(**kwargs)

        self._num_instances = num_instances
        self._num_vertices = num_vertices
        self._num_edges = num_edges
        self._edge_size = edge_size
        self._seed = seed

    def __next__(self):
        if self._num_instances == 0:
            raise StopIteration
        self._num_instances -= 1
        np.random.seed(self._seed)
        instance = Instance(self._generate_edges())
        self._seed = np.random.randint(1 << 32)
        return instance

    def _generate_edges(self):
        edges = []
        while len(edges) < self._num_edges:
            edge = set()
            while len(edge) < self._edge_size:
                edge.add(np.random.randint(self._num_vertices))
            edges.append(tuple(edge))
        return edges


class CNFSatDataset(BaseDataset):
    """
    Generates an instance for CNF satisfiability problem.
    """

    def __init__(self, num_instances, num_variables, num_clauses, clause_size, require_solution=False, seed=0, **kwargs):
        """
        Parameters:
        -----------
        num_instances: int
            the number if instances

        num_variables: int
            the number of variables in each instance

        num_clauses: int
            the number of clauses in each instance

        clause_size: int
            the number of variables in each clause

        require_solution: bool
            whether an instance is obliged to have solution

        seed: int
            random seed
        """
        super(CNFSatDataset, self).__init__(**kwargs)

        self._num_instances = num_instances
        self._num_variables = num_variables
        self._num_clauses = num_clauses
        self._clause_size = clause_size
        self._require_solution = require_solution
        self._seed = seed

        assert self._clause_size <= self._num_variables, 'Clause size must be not greater than the number of variables.'

    def expected_hs_size(self):
        return self._num_variables

    def __next__(self):
        if self._num_instances == 0:
            raise StopIteration
        self._num_instances -= 1
        np.random.seed(self._seed)
        instance = Instance(self._generate_edges())
        self._seed = np.random.randint(1 << 32)
        return instance

    def _generate_edges(self):
        def get_vertex(variable, is_negated):
            if is_negated:
                return variable + self._num_variables
            return variable

        edges = []

        for variable in range(self._num_variables):
            edges.append((get_vertex(variable, False), get_vertex(variable, True)))

        if self._require_solution:
            solution = np.random.randint(0, 2, size=self._num_variables)

        for _ in range(self._num_clauses):
            all_variables = list(range(self._num_variables))
            np.random.shuffle(all_variables)
            clause = all_variables[:self._clause_size]
            if self._require_solution:
                edge = tuple(map(lambda v: get_vertex(v, solution[v]), clause))
                index = np.random.randint(len(edge))
                variable = clause[index]
                edge = edge[:index] + (get_vertex(variable, not solution[variable]),) + edge[index + 1:]
            else:
                edge = tuple(map(lambda v: get_vertex(v, np.random.randint(2)), clause))
            edges.append(edge)

        return edges
