from itertools import combinations


def find_3paths(graph):
    paths = []
    for vertex, neighbors in enumerate(graph):
        for pair in combinations(neighbors, 2):
            if pair[0] not in graph[pair[1]]:
                paths.append((pair[0], vertex, pair[1]))
    return paths


def get_unique_edges(edges):
    return list(map(tuple, set(map(frozenset, edges))))
