from collections import Counter, deque
import json
import os

class Instance(object):
    def __init__(self, edges, meta=None):
        """
        Parameters:
        -----------

        edges: list of tuples or str
            instance edges if the parameter list of tuples,
            else the instance is read from the file
        """
        if isinstance(edges, str):
            assert meta is None, 'Instance meta-data must be in a file.'
            self._load(edges)
            self._source = f"{os.path.basename(edges)[:-len('.hyp')]}"
            return

        self._edges = edges
        self._meta = meta
        self._source = "raw"

    def __eq__(self, other):
        return (set(self._edges), self._meta) == (set(other._edges), other._meta)

    def __repr__(self):
        if self._meta is not None:
            return repr((self._edges, self._meta))
        return repr(self._edges)

    def __iter__(self):
        return iter(self.edges)

    def __len__(self):
        return len(self.edges)

    @property
    def edges(self):
        return self._edges

    @property
    def meta(self):
        return self._meta

    @property
    def source(self):
        return self._source

    def save(self, path):
        self._check_graph_extension(path)
        self._save_edges(path)
        self._save_meta(self._get_meta_path(path))

    def _save_edges(self, path):
        with open(path, 'w') as f:
            for edge in iter(self):
                f.write(' '.join(map(str, edge)) + '\n')

    def _save_meta(self, path):
        meta = self.meta
        if meta is None:
            if os.path.isfile(path):
                os.remove(path)
            return
        with open(path, 'w') as f:
            json.dump(meta, f)

    def _load(self, path):
        self._check_graph_extension(path)
        self._load_edges(path)
        self._load_meta(self._get_meta_path(path))

    def _load_edges(self, path):
        with open(path) as f:
            self._edges = [tuple(map(int, line.split())) for line in f]

    def _load_meta(self, path):
        if not os.path.isfile(path):
            self._meta = None
            return
        with open(path) as f:
            self._meta = json.load(f)

    def _check_graph_extension(self, path):
        assert path.endswith('.hyp'), 'Graph file must have .hyp extension.'

    def _get_meta_path(self, edges_path):
        return edges_path[: -len('.hyp')] + '.json'

    def get_dual(self):
        max_vertex = -1
        for edge in self:
            max_vertex = max(max_vertex, max(edge))
        edges = [[] for i in range(max_vertex + 1)]
        for edge_id, edge in enumerate(self):
            for vertex in edge:
                edges[vertex].append(edge_id)
        return Instance(list(map(tuple, edges)))


class InstanceStatist(object):
    def __init__(self, custom_stats={}):
        """
        Parameters:
        -----------
        custom_stats: str->function dict (function must take Instance as a parameter)
            custom statistics to add to the answer
            default statistics with coinciding names will be overwritten by custom
        """
        self._custom_stats = custom_stats

    def __call__(self, instance):
        num_edges = len(instance)
        vertex_degrees = Counter()
        for edge in instance:
            for vertex in edge:
                vertex_degrees[vertex] += 1
        num_vertices = len(vertex_degrees)
        c4count, p3count = self._count_configurations(instance)
        complexity = self._calc_complexity(instance)

        result = {'num_edges': num_edges,
                  'num_vertices': num_vertices,
                  'max_edge_size': max(map(len, instance.edges)),
                  'vertex_edge_ratio': num_vertices / num_edges,
                  'average_degree': sum(vertex_degrees.values()) / num_vertices,
                  'locality': 4 * c4count / max(p3count, 1),
                  'complexity': complexity}
        result.update({name: statistic(instance) for name, statistic in self._custom_stats.items()})
        return result

    def _count_configurations(self, instance):
        c4count = 0
        p3count = 0
        dual_edges = instance.get_dual().edges
        for edge in instance:
            for vertex in edge:
                p3count += (len(dual_edges[vertex]) - 1) * (len(edge) - 1)
        vertex_pairs = Counter()
        for edge in instance:
            for i in range(len(edge)):
                for j in range(i):
                    pair = (edge[i], edge[j])
                    if pair[0] > pair[1]:
                        pair = pair[1], pair[0]
                    c4count += vertex_pairs[pair]
                    vertex_pairs[pair] += 1
        return c4count, p3count

    def _calc_complexity(self, instance):
        return max(map(len, self._find_connected_components(instance)))

    def _find_connected_components(self, instance):
        vertices = set()
        for edge in instance.edges:
            vertices.update(set(edge))
        dual = instance.get_dual()

        components = []
        used = set()
        for vertex in vertices:
            if vertex in used:
                continue
            components.append(self._find_all_connected(instance, dual, vertex))
            used.update(components[-1])
        return components

    def _find_all_connected(self, instance, dual, vertex):
        used = set()
        queue = deque([vertex])
        while len(queue) > 0:
            vertex = queue.popleft()
            for edge_id in dual.edges[vertex]:
                edge = instance.edges[edge_id]
                neighbors = {neighbor for neighbor in edge if neighbor not in used}
                queue.extend(neighbors)
                used.update(neighbors)
        return used
