import copy
from collections import defaultdict

from hs_lintimespace.clib import CWeiheReducer
from hs_lintimespace.dataset.helpers import get_unique_edges
from hs_lintimespace.dataset.instance import Instance

from hs_lintimespace.reducer.base import BaseReducer


class IdentityReducer(BaseReducer):
    """
    Returns a duplicate of an instance.
    """

    def _call(self, instance):
        return copy.deepcopy(instance), {}


class PipelineReducer(BaseReducer):
    """
    Runs several redusers in line.
    """

    def __init__(self, reducers, **kwargs):
        """
        Parameters:
        -----------
        reducers: list of BaseReducer's
            reducers to run in order
        """
        super(PipelineReducer, self).__init__(**kwargs)
        self._reducers = list(reducers)

    def __call__(self, instance, shuffle=False, **kwargs):
        info = {'timeout': []}
        for stage, reducer in enumerate(self._reducers):
            if shuffle:
                instance = self._shuffle_instance(instance)
            instance, info[stage] = reducer(instance, shuffle=False, **kwargs)
            if info[stage].get('timeout', False):
                info['timeout'].append(stage)
        return instance, info


class NoEmptyReducer(BaseReducer):
    """
    Removes empty edges.
    """

    def _call(self, instance):
        return Instance([edge for edge in instance if edge], copy.deepcopy(instance.meta)), {}


class UniqueReducer(BaseReducer):
    """
    Leaves only unique edges preserving order of the first occurences.
    """

    def _call(self, instance):
        return Instance(get_unique_edges(instance.edges), copy.deepcopy(instance.meta)), {}


class SupersetReducer(BaseReducer):
    """
    Leaves only unique edges preserving order of the first occurences.
    """

    def _call(self, instance):
        edges = sorted(get_unique_edges(instance.edges), key=len)
        keep = []
        for edge in edges:
            keep.append(edge)
            superset = set(edge)
            for subedge in edges:
                if len(subedge) == len(edge):
                    break
                if set(subedge).issubset(superset):
                    keep.pop()
                    break
        return Instance(keep, copy.deepcopy(instance.meta)), {}


class WeiheReducer(BaseReducer):
    """
    An implementation of Weihe's algorithm
    from https://www.researchgate.net/publication/2662996.
    """

    def __init__(self, **kwargs):
        super(WeiheReducer, self).__init__(**kwargs)
        self._reducer = CWeiheReducer()

    def _call(self, instance):
        edges, info = self._reducer(instance.edges)
        return Instance(edges, copy.deepcopy(instance.meta)), info
