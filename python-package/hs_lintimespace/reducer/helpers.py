from collections import Counter


def calc_hs_lowerbound(instance):
    lowerbound = 0
    used = set()
    for edge in instance:
        if all(vertex not in used for vertex in edge):
            lowerbound += 1
            used.update(edge)
    return lowerbound


def calc_hs_upperbound(instance):
    hs = set()
    for edge in instance:
        assert edge, "No hitting set exists due to an empty edge."
        if all(vertex not in hs for vertex in edge):
            hs.add(edge[0])
    return len(hs)


def calc_hs_upperbound_weighted(instance):
    deg = Counter()
    for edge in instance:
        for vertex in edge:
            deg[vertex] += 1
    dual = instance.get_dual()

    hit_edges = set()
    hs = set()
    while len(deg) > 0:
        vertex, vertex_deg = deg.most_common(1)[0]
        del deg[vertex]
        if vertex_deg == 0:
            break

        hs.add(vertex)
        for edge_id in dual.edges[vertex]:
            if edge_id in hit_edges:
                continue
            hit_edges.add(edge_id)
            for neighbor in instance.edges[edge_id]:
                deg[neighbor] -= 1

    return len(hs)


def bruteforce_hitting_sets(instance):
    hitting_sets = set()
    current_set = set()

    def recursive(edges):
        if not edges:
            hitting_sets.add(frozenset(current_set))
            return
        edge = edges[0]
        for vertex in edge:
            if vertex in current_set:
                recursive(edges[1:])
                return
        for vertex in edge:
            current_set.add(vertex)
            recursive(edges[1:])
            current_set.remove(vertex)

    recursive(instance.edges)
    return hitting_sets
