import copy
import math

from hs_lintimespace.clib import (CFafianieKratschReducer, CVanBevernReducer,
                                  CFafianieKratschHashingReducer, CVanBevernHashingReducer,
                                  CFafianieKratschMappingReducer, CVanBevernMappingReducer,
                                  CFafianieKratschWithTrieReducer, CVanBevernWithTrieReducer,
                                  CFafianieKratschWithMapTrieReducer, CVanBevernWithMapTrieReducer)
from hs_lintimespace.dataset.instance import Instance

from hs_lintimespace.reducer.base import BaseReducer
from hs_lintimespace.reducer.helpers import calc_hs_upperbound


class BoundHSReducer(BaseReducer):
    """
    Abstact reducer for algorithms parameterized by the hitting set size.
    """

    def __init__(self, reducer, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        reducer: class
            a reducer to run for each instance, must have a __call__(self, instance, max_hitting_set) method

        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(BoundHSReducer, self).__init__(**kwargs)

        self._reducer = reducer

        if max_hitting_set is None:
            self._max_hitting_set = calc_hs_upperbound
        elif isinstance(max_hitting_set, int):
            self._max_hitting_set = lambda instance: max_hitting_set
        else:
            self._max_hitting_set = max_hitting_set

        if max_all_subsets_edge is None:
            self._max_all_subsets_edge = lambda instance: max(map(len, instance), default=0)
        elif isinstance(max_all_subsets_edge, int):
            self._max_all_subsets_edge = lambda instance: max_all_subsets_edge
        else:
            self._max_all_subsets_edge = max_all_subsets_edge

    def _call(self, instance):
        edges, info = self._reducer(instance.edges,
                                    self._max_hitting_set(instance),
                                    self._max_all_subsets_edge(instance))
        return Instance(edges, copy.deepcopy(instance.meta)), info


class FafianieKratschReducer(BoundHSReducer):
    """
    An implementation of Fafianie and Kratsch algorithm
    from https://arxiv.org/abs/1504.08235 with a memory optimization.
    """

    def __init__(self, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(FafianieKratschReducer, self).__init__(reducer=CFafianieKratschReducer(),
                                                     max_hitting_set=max_hitting_set,
                                                     max_all_subsets_edge=max_all_subsets_edge,
                                                     **kwargs)


class VanBevernReducer(BoundHSReducer):
    """
    An implementation of van Bevern's algorithm
    from https://arxiv.org/abs/1112.2310 with a memory optimization.
    """

    def __init__(self, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(VanBevernReducer, self).__init__(reducer=CVanBevernReducer(),
                                               max_hitting_set=max_hitting_set,
                                               max_all_subsets_edge=max_all_subsets_edge,
                                               **kwargs)


class FafianieKratschHashingReducer(BoundHSReducer):
    """
    An implementation of Fafianie and Kratsch algorithm
    from https://arxiv.org/abs/1504.08235 with a hash table.
    """

    def __init__(self, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(FafianieKratschHashingReducer, self).__init__(reducer=CFafianieKratschHashingReducer(),
                                                            max_hitting_set=max_hitting_set,
                                                            max_all_subsets_edge=max_all_subsets_edge,
                                                            **kwargs)


class VanBevernHashingReducer(BoundHSReducer):
    """
    An implementation of van Bevern's algorithm
    from https://arxiv.org/abs/1112.2310 with a hash table.
    """

    def __init__(self, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(VanBevernHashingReducer, self).__init__(reducer=CVanBevernHashingReducer(),
                                                      max_hitting_set=max_hitting_set,
                                                      max_all_subsets_edge=max_all_subsets_edge,
                                                      **kwargs)


class FafianieKratschMappingReducer(BoundHSReducer):
    """
    An implementation of Fafianie and Kratsch algorithm
    from https://arxiv.org/abs/1504.08235 with a map.
    """

    def __init__(self, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(FafianieKratschMappingReducer, self).__init__(reducer=CFafianieKratschMappingReducer(),
                                                            max_hitting_set=max_hitting_set,
                                                            max_all_subsets_edge=max_all_subsets_edge,
                                                            **kwargs)


class VanBevernMappingReducer(BoundHSReducer):
    """
    An implementation of van Bevern's algorithm
    from https://arxiv.org/abs/1112.2310 with a map.
    """

    def __init__(self, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(VanBevernMappingReducer, self).__init__(reducer=CVanBevernMappingReducer(),
                                                      max_hitting_set=max_hitting_set,
                                                      max_all_subsets_edge=max_all_subsets_edge,
                                                      **kwargs)


class FafianieKratschWithTrieReducer(BoundHSReducer):
    """
    An implementation of Fafianie and Kratsch algorithm
    from https://arxiv.org/abs/1504.08235 with a trie.
    """

    def __init__(self, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(FafianieKratschWithTrieReducer, self).__init__(reducer=CFafianieKratschWithTrieReducer(),
                                                             max_hitting_set=max_hitting_set,
                                                             max_all_subsets_edge=max_all_subsets_edge,
                                                             **kwargs)


class VanBevernWithTrieReducer(BoundHSReducer):
    """
    An implementation of van Bevern's algorithm
    from https://arxiv.org/abs/1112.2310 with a trie.
    """

    def __init__(self, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(VanBevernWithTrieReducer, self).__init__(reducer=CVanBevernWithTrieReducer(),
                                                       max_hitting_set=max_hitting_set,
                                                       max_all_subsets_edge=max_all_subsets_edge,
                                                       **kwargs)


class FafianieKratschWithMapTrieReducer(BoundHSReducer):
    """
    An implementation of Fafianie and Kratsch algorithm
    from https://arxiv.org/abs/1504.08235 with a trie with maps in nodes.
    """

    def __init__(self, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(FafianieKratschWithMapTrieReducer, self).__init__(reducer=CFafianieKratschWithMapTrieReducer(),
                                                                max_hitting_set=max_hitting_set,
                                                                max_all_subsets_edge=max_all_subsets_edge,
                                                                **kwargs)


class VanBevernWithMapTrieReducer(BoundHSReducer):
    """
    An implementation of van Bevern's algorithm
    from https://arxiv.org/abs/1112.2310 with a trie with maps in nodes.
    """

    def __init__(self, max_hitting_set=None, max_all_subsets_edge=None, **kwargs):
        """
        Parameters:
        -----------
        max_hitting_set: int or function (default is lib.reducer.helpers.calc_hs_upperbound)
            if int, is a hitting set upper bound
            if function, is a function taking instance and returning a hitting set upper bound

        max_all_subsets_edge: int or function (default is max edge size)
            if int, is a max size of an edge to count all subsets
            if function, is a function taking instance and returning such size
            for all edge of greater sizes subsets will be count as intersections,
            the answer will be approximate
        """
        super(VanBevernWithMapTrieReducer, self).__init__(reducer=CVanBevernWithMapTrieReducer(),
                                                          max_hitting_set=max_hitting_set,
                                                          max_all_subsets_edge=max_all_subsets_edge,
                                                          **kwargs)
