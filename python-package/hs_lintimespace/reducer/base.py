from contextlib import contextmanager
import multiprocessing
import signal

import numpy as np

from hs_lintimespace.dataset.instance import Instance


@contextmanager
def timeout_context(time):
    def raise_timeout(signum, frame):
        raise TimeoutError

    signal.signal(signal.SIGALRM, raise_timeout)
    signal.setitimer(signal.ITIMER_REAL, time)

    try:
        yield
    except TimeoutError:
        pass
    finally:
        signal.signal(signal.SIGALRM, signal.SIG_IGN)


class BaseReducer(object):
    def __init__(self, shuffle_seed=0, **kwargs):
        self._shuffle_seed = shuffle_seed

    def __call__(self, instance, timeout=None, shuffle=False):
        """
        Returns a tuple (reduced_instance, info_dict)
        """
        if shuffle:
            instance = self._shuffle_instance(instance)

        """
        if timeout is None:
            return self._call(instance)

        with timeout_context(timeout):
            return self._call(instance)

        return instance, {'timeout': True}
        """

        if timeout is None:
            return self._call(instance)

        def call_thread(instance, return_dict):
            reduced_instance, info_dict = self._call(instance)
            return_dict['reduced_instance'] = reduced_instance
            return_dict['info_dict'] = info_dict

        returned_value = []
        manager = multiprocessing.Manager()
        return_dict = manager.dict()
        process = multiprocessing.Process(target=call_thread, args=(instance, return_dict))
        process.start()
        process.join(timeout=timeout)
        if process.is_alive():
            process.terminate()
            process.join()
            return instance, {'timeout': True}
        return return_dict['reduced_instance'], return_dict['info_dict']

    def _shuffle_instance(self, instance):
        np.random.seed(self._shuffle_seed)
        self._shuffle_seed = np.random.randint(1 << 32)
        permutation = np.arange(len(instance), dtype=int)
        np.random.shuffle(permutation)
        shuffled_edges = []
        for i in permutation:
            shuffled_edges.append(instance.edges[i])

        return Instance(shuffled_edges, instance.meta)

    def _call(self, instance):
        """
        Must return a tuple (reduced_instance, info_dict)
        """
        raise NotImplemented
